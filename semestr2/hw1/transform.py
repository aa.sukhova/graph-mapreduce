from mrjob.job import MRJob
from mrjob.step import MRStep
import csv

import string
import random
import re

def id_generator(size = 6, chars = string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def check_delimiter(string):
	if string == '\\t':
		string = '\t'
	return string

class CsvToJson(MRJob):

	def configure_args(self):
	    super(CsvToJson, self).configure_args()
	    self.add_passthru_arg('--delimiter')
	    self.add_passthru_arg('--start-string')
	    self.add_passthru_arg('--source')

	def steps(self):
		return [
			MRStep(
				mapper=self.mapper_parse_by_delimiter,
				reducer=self.reducer
			)
		]

	def mapper_parse_by_delimiter(self, _, line):
		delimiter = check_delimiter(self.options.delimiter)

		read = csv.reader([line], delimiter = delimiter);

		line_id = id_generator(5)
		for elem in read:
			yield line_id, elem

	def reducer(self, line_id, values):
		delimiter = self.options.delimiter
		read = csv.reader([self.options.start_string])

		names = list(read)[0]
		data = list(values)

		if (names[0] == data[0][0]):
			return

		json = {}
		cnt = 0
		for elem in data[0]:
			json[names[cnt]] = elem
			cnt = cnt + 1

		json["source"] = self.options.source

		yield line_id, json


if __name__ == '__main__':
	CsvToJson.run()
