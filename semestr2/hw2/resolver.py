from mrjob.job import MRJob
from mrjob.step import MRStep
import json

import string
import re
import random

import enchant

def id_generator(size = 6, chars = string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

class Resolver(MRJob):
	scheme = ['title', 'year', 'category', 'rank', 'oscar',
			  'director', 'actors', 'country',
			  'source', 'description', 'imdb']

	def steps(self):
		return [
			MRStep(
				mapper=self.mapper,
				reducer=self.reducer
			),
			MRStep(
				reducer=self.reducer2
			),
		]

	def unify_key(self, key):
		key = key.lower()
		return key

	def unify_title(self, key):
		key = key.lower()
		res = ''
		for ch in key:
			if ch.isalpha() or ch.isnumeric():
				res += ch

		return res


	def mapper(self, _, line):
		data = json.loads(line)
		line_id = id_generator(5)

		format_data = {}

		for key, value in data.items():
			key = self.unify_key(key)
			format_data[key] = value


		target_scheme = {}

		for name in self.scheme:
			target_scheme[name] = ''

		for key, value in format_data.items():
			for field in self.scheme:
				if key.find(field) != -1:
					if target_scheme[field] != '':
						target_scheme[field] += ',' + value
					else:
						target_scheme[field] = value

					break

		yield None, (line_id, target_scheme)
		yield None, (line_id, (self.unify_title(target_scheme['title']),
							   target_scheme['year'],
							   target_scheme['source']))


	def equal(self, one, two):
		(title1, year1, source1) = one
		(title2, year2, source2) = two

		x_len = min(len(title1), len(title2))
		x = enchant.utils.levenshtein(title1, title2)

		y = 0
		if (year1.find(year2) != -1) or (year2.find(year1) != -1):
			y = 1

		return (x <= 4 and (x < x_len / 3)) and (y == 1) and (source1 != source2)

	def reducer(self, _, info):
		data = []
		idents = {}
		titles = {}
		cnt = 0;

		for elem in info:
			if len(elem[1]) > 3:
				yield elem[0], elem[1]
				x = 0
			else:
				data.append(elem)
				idents[elem[0]] = cnt
				titles[elem[0]] = elem[1][0]
				cnt = cnt + 1

		for i in range(0, len(data)):
			for j in range(i + 1, len(data)):

				line_id_i = data[i][0]
				line_id_j = data[j][0]

				if self.equal(data[i][1], data[j][1]):
					idents[line_id_j] = idents[line_id_i]

		for line_id, ident in idents.items():
			yield line_id, ident

	def reducer2(self, line_id, info):
		data = list(info)
		if type(data[0]) != type(dict()):
			data[1]['identifier'] = data[0]
			yield 1, data[1]
		else:
			data[0]['identifier'] = data[1]
			yield 1, data[0]

if __name__ == '__main__':
	Resolver.run()
