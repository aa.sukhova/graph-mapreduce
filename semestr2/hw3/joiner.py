from mrjob.job import MRJob
from mrjob.step import MRStep
import json

import string
import re
import random

class Join(MRJob):
	def steps(self):
		return [
			MRStep(
				mapper=self.mapper,
				reducer=self.reducer
			)
		]

	def mapper(self, _, line):
		data = json.loads(line)
		ident = data['identifier']
		data.pop('identifier')
		yield ident, data

	def unify_string(self, key):
		key = key.lower()
		res = ''
		for ch in key:
			if ch.isalpha() or ch.isnumeric():
				res += ch

		return res

	def join(self, data):
		res = {}
		data1 = data[0]
		data2 = data[1]

		for key, value1 in data1.items():
			value2 = self.unify_string(str(data2[key]))
			value1 = self.unify_string(str(value1))

			if value1 == '':
				res[key] = data2[key]
			elif value2 == '':
				res[key] = data1[key]
			elif value1.find(value2) != -1:
				res[key] = data1[key]
			elif value2.find(value1) != -1:
				res[key] = data2[key]
			else:
				res[key] = data1[key] + '; ' + data2[key]

		return res


	def reducer(self, id, info):
		data = list(info)

		if len(data) == 1:
			yield 1, data[0]
		else:
			yield 1, self.join(data)


if __name__ == '__main__':
	Join.run()
