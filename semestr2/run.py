import os

run1 = 'python3 hw1/transform.py hw0/1.csv \
	--delimiter "," \
	--source "Best Guardian" \
	--start-string "id,ranking_in_category_guardian,title,director,leading_actors,year_released,no_of_oscars won,IMDB_link,guardian_film_page,country,category_guardian,list_source,list_year,note" \
	|  awk \'{for(i=2;i<=NF;i++) printf $i" "; print ""}\' \
	> hw1/1.json'

run2 = 'python3 hw1/transform.py hw0/2.csv \
	--delimiter "\t" \
	--source "Best On Netflix" \
	--start-string "Title,Year,Description from Thrillest" \
	|  awk \'{for(i=2;i<=NF;i++) printf $i" "; print ""}\' \
	> hw1/2.json'

os.system(run1)
os.system(run2)

run3 = 'python3 hw2/resolver.py hw1/*.json \
	|  awk \'{for(i=2;i<=NF;i++) printf $i" "; print ""}\' \
	> hw2/result.json'

os.system(run3)

run4 = 'python3 hw3/joiner.py hw2/*.json \
	|  awk \'{for(i=2;i<=NF;i++) printf $i" "; print ""}\' \
	> hw3/result.json'

os.system(run4)
