
## Compute TF-IDF index for line

https://en.wikipedia.org/wiki/Tf–idf


### Run:
```
python3  line_tf_idf.py  <directory or file>  --search-line <"some line for compute tf-idf">
```
### For example:
```
python3  line_tf_idf.py  files/  --search-line  "google find something"
```
