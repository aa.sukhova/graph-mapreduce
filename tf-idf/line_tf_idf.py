from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob.compat import jobconf_from_env

from math import log
import re

parse_word = re.compile(r"[\w']+")

class GetLineTfIdf(MRJob):

	def configure_args(self):
	    super(GetLineTfIdf, self).configure_args()
	    self.add_passthru_arg('--search-line')

	def steps(self):
		return [
			MRStep(
				mapper=self.mapper_parse_line,
				combiner=self.reducer_word_count_in_doc,
				reducer=self.reducer_word_count_in_doc,
			),
			MRStep(
				mapper=self.mapper_count_number_of_docs,
				reducer=self.reducer_count_number_of_docs,
			),
			MRStep(
				reducer=self.reducer_number_words_in_doc,
			),
			MRStep(
				reducer=self.reducer_word_tf_idf,
			),
			MRStep(
				reducer=self.reducer_line_tf_idf,
			),
		]

	def mapper_parse_line(self, _, line):
		doc = jobconf_from_env('map.input.file')
		for word in parse_word.findall(line):
			yield (word, doc), 1
	
	def reducer_word_count_in_doc(self, word_doc, count):
		yield word_doc, sum(count)


	def mapper_count_number_of_docs(self, word_doc, freq):
		yield None, (word_doc[0], word_doc[1], freq)

	def reducer_count_number_of_docs(self, _, values):
		docs_name = []
		save_values = []

		for word, doc, freq in values:
			if doc not in docs_name:
				docs_name.append(doc)

			save_values.append([word, doc, freq])

		for word, doc, freq in save_values:
			yield doc, (word, freq, len(docs_name))


	def reducer_number_words_in_doc(self, doc, values):
		nwords = 0
		save_values = []

		for word, freq, ndocs in values:
			save_values.append([word, freq, ndocs])
			nwords += freq

		search_line = parse_word.findall(self.options.search_line)

		for word, freq, ndocs in save_values:
			if word in search_line:
				yield word, (doc, freq, nwords, ndocs)



	def reducer_word_tf_idf(self, word, values):
		word_in_docs = 0
		number_of_all_docs = 0

		save_values = []

		for doc, freq, nwords, ndocs in values:
			save_values.append([doc, freq, nwords])

			word_in_docs += 1
			number_of_all_docs = ndocs
			

		for doc, freq, nwords in save_values:
			tf_idf = (freq / nwords) * log(number_of_all_docs / word_in_docs)

			yield doc, tf_idf


	def reducer_line_tf_idf(self, doc, values):
		sum_tf_idf = 0
		count = 0
		for tf_idf in values:
			sum_tf_idf += tf_idf
			count += 1

		yield doc, sum_tf_idf / count



if __name__ == '__main__':
	GetLineTfIdf.run()
